================
使用手冊
================
\ **直讀聖經**\ 在是一個跨平台的應用程式，所以在網頁版，iOS，及 Android 等版本的操作是一致的。本文件的圖例都擷取自 iOS 版操作結果，除了畫面尺寸與少部分的狀態列資訊可能不同，操作步驟與內容在各平台上都是一致的。您可以按照所示畫面練習各項功能。

開啟
=====================
.. _fig-BibleIcon:
.. figure:: _static/images/BibleIcon.png
    :width: 64px
    :alt: Direct Bibles Icon
    :align: center

    直讀聖經圖示

- 以瀏覽器開啟網頁：`首頁 <https://www.bibles.com.tw>`_
- 使用 iOS 或 Android 的應用程式：從兩大官方網路商店安裝本程式後，您可以在手機或平板的應用程式裡找到直讀聖經，一個綠色的古書圖示如右圖 :ref:`fig-BibleIcon`，點開以啟動程式。

讀經首頁
=====================
.. _fig-BibleHomepage:
.. figure:: _static/images/BibleHomepage.jpg
    :width: 260px
    :figwidth: 260px
    :alt: The Bible Reading Page
    :align: center

    讀經首頁

在瀏覽器載入或程式開啟後，程式會直接進入讀經首頁。首次使用，預設為您開啟新約聖經的第一卷，馬太福音，第一章。載入後的畫面便是讀經的版面，只有標題，節號，和經文，如圖 :ref:`fig-BibleHomepage` 沒有其他操作介面，這是直讀聖經的設計特色：

- 只讀經文不玩手機
- 打開直接讀
- 中文讀直的
- 一直讀聖經

頁面的幾個特點包括：

- 隱藏所有不需要一直佔用畫面的元素，保留最大的空間給經文。
- 讀經頁面本身會依照裝置銀幕的大小與方向調整。如果您使用手機或平板，可以試著把您的裝置橫放，頁面會跟著轉向。
- 預設的紙質背景功能在減低純白背景的藍光成分，保護眼睛比較不容易疲累。但仍可以關閉，使用純白背景。
- 字體大小可以在設定頁改變。設定較大字體對眼睛比較輕鬆，較小字體則可以在一頁畫面裡顯示最多的經文。視讀者需求自訂。
- 連續閱讀時，可以關閉節號，行距字距皆可調整，使單頁顯示更多經文。（我們知道您對真理的渴望）
- 可以更換字體：聖經不同於今日快速流逝的大量網路資訊，閱讀過程中，很多時候我們會停在特定的字句上面細細思考。經文的呈現要能經得起凝視，消費型網頁的的文字呈現多數不符合這個需求，所以我們在字體以及版面上，提供您最多的彈性，使您能有舒適的閱讀品質。
- 不過，我們還是需要翻頁查找經文啊！沒問題，請用滑鼠或手指在畫面中間任意點一下，操作用的介面就會顯示出來。

開啟功能表
=====================
除了畫面的左右兩側保留了固定空間作為翻頁按鈕之外，在畫面中間單點一下，即可帶出 :ref:`fig-BibleMainMenu`，上下兩個深色長條區域有不同的元件提供操作本程式所需的各項功能。點一下開啟功能表，再點一下，可以關閉，回到沒有介面的全版讀經頁面。

.. _fig-BibleMainMenu:
.. figure:: _static/images/BibleMainMenu.jpg
    :width: 260px
    :figwidth: 260px
    :alt: The Main Menu
    :align: center

    功能表

翻頁按鈕
=====================
在點擊開啟操作介面時，畫面的兩側分別出現向左和向右箭頭，如圖 :ref:`fig-BiblePageButtons`。這兩個圈起來的箭頭，出現一下馬上就又淡化消失，為的是提醒讀者：按畫面兩側可以翻頁。讀經的時候，您可以用手指捲動頁面，也可以按左右兩邊分別往前或往後捲動一整頁。有效範圍比箭頭的圖樣還大許多，在附近按一下都可以翻頁，不必特別把箭頭點出來。往左翻頁的時候，為了閱讀的銜接，原則上會保留前一個畫面的最後一行作為翻頁後的第一行。
（橫式排版的模式下，沒有左右翻頁的功能，直接上下捲動即可。）

.. _fig-BiblePageButtons:
.. figure:: _static/images/BiblePageButtons.jpg
    :width: 260px
    :figwidth: 260px
    :alt: The Page Buttons
    :align: center

    翻頁按鈕

操作介面主要功能
=====================
操作介面為上下兩個功能表，主要功能分別以紅線和紅字標示出來如下圖 :ref:`fig-BibleMainMenuDesc` 所示。

.. _fig-BibleMainMenuDesc:
.. figure:: _static/images/BibleMainMenuDesc.jpg
    :width: 260px
    :figwidth: 260px
    :alt: The Page Buttons
    :align: center

    介面功能說明

- 標題
    除了顯示程式名稱之外，它還有相當於瀏覽器的『回首頁』的功能，按一下，可以回到新約聖經的最前面。
- 上/下一網頁
    前一章所介紹的閱讀時前後翻頁，是在同一卷聖經裡的連續閱讀時的快速『捲動』，一次往前或往後捲動多行。上功能表裡的這兩個箭頭，功能相當與瀏覽器的往前和往後瀏覽，直接『跳動』到另一個頁面，例如，從設定頁直接回到讀經頁面，從詩篇跳到羅馬書，從和合本跳到文理本。
- 重新整理
    重新整理的圖形按鈕，與瀏覽器的『重新整理』功能一樣，會重新載入當前頁面，並重新顯示。不論當下閱讀的內容是線上即使取得的或是本地儲存供離線使用的，按重新整理都可以更新當下頁面。
- 帳號操作
    點開帳號操作功能，會出現一個子功能表，提供更多帳號相關的操作，包括下載離線版本的聖經。留到後面單節完整說明其中各項。
- 聖經版本，書卷，與篇章
    下功能表中有四個元件，都是閱讀是較常用的功能。前三個分別是版本，書卷，篇章。這三個元件讓讀者可以快速切換到特定的經文篇章，書卷選單像是傳統紙本聖經貼的小耳朵快速索引標籤。不過，因為這裡提供多種聖經版本，所以還需要版本的選項，而篇章也是查找的重要依據，所以把三個最重要的查找功能直接放在第一層的功能表裡，方便使用。點擊下功能表上所顯示的目前版本書卷或篇章，可以帶出該項目的選單，選單內容如下：

    - 版本選單：直讀聖經 V1.1 提供八個版本的聖經，包括
        - 和合本，繁體中文版
        - 新譯本，繁體中文版
        - 文理本，繁體中文版
        - 和合本，簡體中文版
        - 新譯本，簡體中文版
        - KJV, King James Version, 英皇欽定本英文版
        - ESV, English Standard Version, 英文標準本
        - WEB, World English Bible, 世界英文版

        我們同時還在洽談與整理其他版本聖經，尤其是亞洲直式書寫語言的版本。有新版聖經上架的時候，我們會透過程式本身通知註冊用戶。

    - 書卷選單：按照順序列舉39卷舊約和27卷新約，共66卷。選單一開始預設停在馬太福音，往上查找是舊約書卷，往下查找是新約書卷。選單列舉的是完整的書卷名，例如，創世紀，詩篇，啓示錄等，而不採用縮寫，可以一目了然。

    - 篇章選單：直讀聖經的經文是以書卷為單位一次載入。也就是說，當您選取版本或書卷的時候用，程式會一次載入對應版本的書卷，排版顯示。這時候，篇章選單的內容會跟著所選書卷調整，例如，詩篇有 150 篇，約翰福音有 21 章，而腓利門書只有一章。操作上，您會發現與前兩個選單感覺不同，原因是，前兩個選單變動時，程式會載入另外一卷聖經，但選擇篇章時，是在同一份文件裡前後捲動，少了讀檔載入的時間，操作速度比較快。

    以下三個畫面示範上述選單內容，是從 iPhone 擷取的操作實況。由於配合各系統內建的選單元件，所以使用 Android 系統或桌上電腦的瀏覽器時，外觀可能不太一樣，但是內容與使用方法都是相同的。

.. _fig-BibleMenuVersions:
.. figure:: _static/images/BibleMenuVersions.jpg
    :width: 260px
    :figwidth: 260px
    :alt: The Versions Menu
    :align: center

    版本選單

.. _fig-BibleMenuBooks:
.. figure:: _static/images/BibleMenuBooks.jpg
    :width: 260px
    :figwidth: 260px
    :alt: The Books Menu
    :align: center

    書卷選單

.. _fig-BibleMenuChapters:
.. figure:: _static/images/BibleMenuChapters.jpg
    :width: 260px
    :figwidth: 260px
    :alt: The Chapters Menu
    :align: center

    篇章選單

設定
=====================
.. _fig-BibleSettings:
.. figure:: _static/images/BibleSettings.jpg
    :width: 260px
    :figwidth: 260px
    :alt: The SettingsPage
    :align: center

    經文版面設定

主功能表裡最右下角的齒輪形狀圖示，即是設定按鈕，用來設定版面細節。按下該按鈕，會進入 :ref:`fig-BibleSettings` 的頁面如下圖。

這裡提供閱讀版面相關的各種設定，讓您能在自己的手機或電腦上，調整出您讀起來最輕鬆悅目的版面。您也可以完全採用我們事先調整好的預設值，不再做任何調整。您第一次打開這一頁的時候用所顯示的數值，即是對應於您的裝置與語言設定的理想設定。但是您可能會希望讓字體再大一點，行距小一點，等等，都可以在這裡設定。尤其，選用不同的字型閱讀效果完全不同，黑體可能對閱讀速度有幫助，而隸屬讓人心情平穩的體會金句。

您可以大膽地嘗試不同的版面設定參數，並找出您個人最滿意的組合。不用擔心調亂了而找不回來通用的預設值，最底下有一個重設的按鍵，按一下就恢復預設值了。原則上設定好離開這個畫面之後，接下來的閱讀版面都會以此為依據，您不用每次回來調整。但是，當您選擇了另外一個語言版本的聖經時，程式會自動調整到該語系的預設值：例如，繁體和簡體中文的字型原則上是不相同的組合，使用繁體字型顯示簡體版聖經，可能會有一些字是亂碼或空白。而中文書法字體通常不適合顯示英文文件。這些，直讀聖經都會幫您自動微調，但是當您又回到另外一個語系的時候用，如果發現先前的設定跑掉了，請回來這一頁再設定一次。

經文版面設定頁面提供以下這些排版項目：

- 字型
- 字體大小
- 字距
- 行距
- 背景開關
- 節號開關
- 直排橫排開關
- 段落或逐句顯示

支援直式與橫式排版是直讀聖經的特色，節號和分行設定可能是聖經特有的排版項目。除此之外，都是標準的文書處理排版設定。如果您有使用 Word 或 Pages 的文書處理經驗，您應該可以自己很順手地調整。以下我們直接以不同的設定輸出畫面示範。

字型設定
=======================
直讀聖經最早是為繁體中文設計的，後來加入了簡體中文合英文版。程式本身會根據客端裝置所回報的語系設定介面，但不限定可閱讀的聖經版本。例如，以繁體中文為主要介面語言的手機，看到的直讀聖經操作介面是繁體中文版本的（就像您在這份說明書裡看到的所有的截圖），但是不限制讀者閱讀繁體中文，簡體中文，或英文版聖經。不過，字型選單會根據當下的聖經語言版本列舉適合的字型，排除已知不相容的組合，例如，繁體標楷體不適合簡體中文聖經，因為缺很多字，以其他字型替代缺字，閱讀起來仍很不順暢，所以排除。目前繁體中文支援以下字型：

- 預設黑體（裝置內建）
- 預設明體（裝置內建）
- 隸書（華康）
- 仿宋體（華康）
- 標楷體（台灣教育部）
- 標明體（台灣教育部）

以下列舉繁體中文所支援的字體的實例供您對照參考

.. _fig-BibleFontHei:
.. figure:: _static/images/BibleFontHei.jpg
    :width: 260px
    :figwidth: 260px
    :alt: The Font Sans Serif
    :align: center

    預設黑體

.. _fig-BibleFontMing:
.. figure:: _static/images/BibleFontMing.jpg
    :width: 260px
    :figwidth: 260px
    :alt: The Font Serif
    :align: center

    預設明體

.. _fig-BibleFontLishu:
.. figure:: _static/images/BibleFontLishu.jpg
    :width: 260px
    :figwidth: 260px
    :alt: The Font Lishu
    :align: center

    隸書

.. _fig-BibleFontFangSong:
.. figure:: _static/images/BibleFontFangSong.jpg
    :width: 260px
    :figwidth: 260px
    :alt: The Font FangSong
    :align: center

    仿宋體

.. _fig-BibleFontKai:
.. figure:: _static/images/BibleFontKai.jpg
    :width: 260px
    :figwidth: 260px
    :alt: The Font Kai
    :align: center

    標楷體

.. _fig-BibleFontMingEdu:
.. figure:: _static/images/BibleFontMingEdu.jpg
    :width: 260px
    :figwidth: 260px
    :alt: The Font Ming Edu
    :align: center

    標明體


字體大小
=======================
直讀聖經所使用的字體大小的單位是網頁通用的字體尺寸單位，px。雖然非所有的行動裝置都精確地以這個公式換算，但通常 1 px = 1/96 英吋。直讀聖經的中文字預設為 20px，英文這設定為 18px，比一般印刷品的常用大小 16px 略大一些，在普遍的 5 英吋 Full HD (1280x1920) 的手機螢幕上有很好的顯示品質。不過這個設定對於年紀稍長的讀者而言可能仍不是最舒適的閱讀大小，通常設定到 28px 仍有很好的版面品質；若您幫家中長者安裝直讀聖經，設定成 36px，幾乎不用老花眼鏡也可以清楚讀出字體了。以下的範例圖，供您對不同的字體大小有一個基本的概念，實際上仍建議您在自己的裝置上體驗以下，並且設定到最合適的大小。

.. _fig-BibleFontSize20:
.. figure:: _static/images/BibleFontSize20.jpg
    :width: 260px
    :figwidth: 260px
    :alt: Font Size 20
    :align: center

    預設中文字的大小 20px

.. _fig-BibleFontSize28:
.. figure:: _static/images/BibleFontSize28.jpg
    :width: 260px
    :figwidth: 260px
    :alt: Font Size 28
    :align: center

    28px 字體大小

.. _fig-BibleFontSize36:
.. figure:: _static/images/BibleFontSize36.jpg
    :width: 260px
    :figwidth: 260px
    :alt: Font Size 36
    :align: center

    36px 字體大小

如果您的視力很好，想在單頁裡讀到最多的經文，以下是 16px 的字體，真的是紙本書頁的資訊量了。但是如果要稍微長時間閱讀，建議選用大一點的字型。

.. _fig-BibleFontSize16:
.. figure:: _static/images/BibleFontSize16.jpg
    :width: 260px
    :figwidth: 260px
    :alt: Font Size 16
    :align: center

    16px 字體大小

行距字距
=======================
中文字型裡，各個不同的字體，不一定是正方塊字，例如，仿宋體是比較瘦長的字，而隸書則是比較寬扁的字，黑體通常方正滿版，同樣的尺寸單位看起來大很多，標楷體雖然方正但相對較小。所以在更換字型的時候用，您可能會突然覺得版面變得擁擠或鬆散，看起來就是不夠優美，然後歎一口氣：電子書真的只能做到工程師的視覺品質，我還是讀紙本好了。其實是有空間改善的：調整字距行距。這不算特別麻煩，畢竟調整一次以後就留著使用了，就像調整您辦公桌椅鍵盤和螢幕高度一樣，舒適很重要。直讀聖經已經針對這些字型特性做過微調，您仍可以根據個人喜好，透過這個介面調整。我們的目的還是一樣的，要讓它盡可能和紙本書一樣好讀。

- \ **行距**\

前一小節裡有個 36px 字的範例，只能讀到兩節經文，實在很可惜。但是，如果我們同時調整以下行距，從預設的 180% 跳著為 130% 情形就改變了，如下圖，從 5 行字變 7 行字，改善很多。

.. _fig-BibleFontSize36Condense:
.. figure:: _static/images/BibleFontSize36Condense.jpg
    :width: 260px
    :figwidth: 260px
    :alt: Font Size 16
    :align: center

    36px 字體 130% 行距

直讀聖經的行距，即是橫式文件的『行高』的概念，直式排版時，所指的是每一行的寬度，相對於字體大小的百分比。200% 即是西式文件裡常用的 double space，兩倍高或兩倍寬的意思。直讀聖經預設使用 180％ 的行距。有效行距我們限制為 100% ~ 300%，建議值在 150％ ~ 200%，可以有比較舒適的版面。

- \ **字距**\

以我們所採用的隸書為例，因為比較扁的關係，程式會自動縮緊行距，否則看起來很鬆散。微調後，版面就好看很多。字距的設定單位為 px，範圍為 -5px ~ 5px。負值代表字與字互相靠攏，正值代表放寬距離。範圍不大，因為我們要幫您把關，免得不小心擠到把字都疊在一起了，或讓經文放鬆到變成填字遊戲的棋盤格子了。

這一節的基本原則：您如果對行距字距有概念，透過這些微調可以得到更滿意的版面；如果您並不十分理解，沒有關係，不理會這些設定也可以；但也都可以試試看，搞不定再重設回預設值就好了。我們的設想是，一年裡，您可能會花數十數百小時在讀經，我們希望在各個細節上給您最舒適的閱讀經驗；我們理解，坐在上有一粒花生的座椅上飛行十個小時，是多麼可怕的折磨。

.. _fig-BibleLayoutSample:
.. figure:: _static/images/BibleLayoutSample.jpg
    :width: 260px
    :figwidth: 260px
    :alt: Character Space 0
    :align: center

    排版對照 A

.. _fig-BibleLayoutLoose:
.. figure:: _static/images/BibleLayoutLoose.jpg
    :width: 260px
    :figwidth: 260px
    :alt: Character Space 1px
    :align: center

    排版對照 B

如果上面這兩張範例您很明顯的比較偏好某一張，那麼以您的視覺敏感度，值得為自己微調一個最佳化的版面。您也許會問，為什麼不是由直讀聖經智慧且自動地調整呢？第一，直讀聖經目前沒有人工智慧成份，其次，我們已經做過品質最佳化了，但仍保留品味空間給您自行發揮。
以上兩個版面的設定值分別是

:ref:`fig-BibleLayoutSample`
    - 字體：標楷體
    - 大小：21px
    - 字距：-1px
    - 行距：160%

:ref:`fig-BibleLayoutLoose`
    - 字體：標楷體
    - 大小：20px
    - 字距：0px
    - 行距：180%

我們不能斷言那一個版面比較美觀，但是如果其中的細微差異確實影響您的閱讀舒適度，那麼我們開放的這些微調就很有意義了，如前面所說的辦公椅，您不會一直調整坐墊的高度和椅背的距離與角度，但是您還是會希望能需要的時候可以調整。

其他版面選項
=======================
- \ **使用或關閉背景**\

.. _fig-ComparisonBackground:
.. figure:: _static/images/BibleComparisonBackground.jpg
    :width: 570px
    :figwidth: 570px
    :alt: With/without background
    :align: center

    背景開關對照

- \ **顯示或關閉節號**\

.. _fig-ComparisonNumber:
.. figure:: _static/images/BibleComparisonNumber.jpg
    :width: 570px
    :figwidth: 570px
    :alt: With/without verse numbers
    :align: center

    節號開關對照

- \ **直式與橫式排版**\

.. _fig-ComparisonHorizontal:
.. figure:: _static/images/BibleComparisonHorizontal.jpg
    :width: 570px
    :figwidth: 570px
    :alt: Vertical / Horizontal Layout Comparison
    :align: center

    直橫排版對照

- \ **段落模式或逐句分行**\

.. _fig-ComparisonPerVerse:
.. figure:: _static/images/BibleComparisonPerVerse.jpg
    :width: 570px
    :figwidth: 570px
    :alt: Paragaphed / Per Verse
    :align: center

    分段分行對照

英文版範例
=======================
基於中文本身的特質與翻譯的限制，在閱讀許多經句的時候用必須嚴謹對照多個版本，不能單憑自己的字面理解看待，能對照希伯來或希臘文原文當然很好，不過要學習這兩個語言到不會誤解經文原意的程度，是一個長期的學習歷程。不害怕英文的朋友們，參考英文版聖經便成了一個可行的選項。所以我們也收錄了幾個通用的英文版聖經，方便交叉閱讀。英文，就不適合直讀了，所以我們特別把英文版的版面範例也列舉在手冊裡，供您參考。

這是英文版的標準版面

.. _fig-BibleLayoutEngSample:
.. figure:: _static/images/BibleLayoutEngSample.jpg
    :width: 260px
    :figwidth: 260px
    :alt: A KJV Sample Page
    :align: center

    A KJV Sample Page

這是一個莎草紙字體樣本。其實莎草紙字體的閱讀效率可能比較低，但是實在很適合拿來做經文單張。

.. _fig-BibleLayoutEngPapyrus:
.. figure:: _static/images/BibleLayoutEngPapyrus.jpg
    :width: 260px
    :figwidth: 260px
    :alt: A KJV Sample Page With Papyrus Font
    :align: center

    A Papyrus Font Sample

（小秘訣：關閉背景，選擇所要的經文與版面，直接從手機上截圖，可以用來做經文單張。如果經文單張的輸出使用戶們普遍需要的功能，我們很樂意繼續努力寫作，把它加進直讀聖經。您可以來信告訴我們，您支持開發這個功能。）

當您選取英文版聖經的時候，程式會自動切換以橫式排版顯示。但英文做直式排版長什麼樣子呢，如下圖。不過以下這三張都是截圖後逆時針旋轉90度的影像。如果您要在手機上讀這類的頁面，請先關閉手機的螢幕自動旋轉設定，就可以讀到比較寬版的英文頁面。

.. _fig-BibleLayoutEngVertical:
.. figure:: _static/images/BibleLayoutEngVertical.jpg
    :width: 455px
    :figwidth: 455px
    :alt: A KJV Sample Page Rotated
    :align: center

    A KJV Sample Page Rotated

上圖是蘋果手機的預設黑體字，通稱 Sans Serif Fonts。下圖則是對應於明體的 Serif 字型。

.. _fig-BibleLayoutEngSerif:
.. figure:: _static/images/BibleLayoutEngSerif.jpg
    :width: 455px
    :figwidth: 455px
    :alt: A KJV Sample Page Rotated with Serif Font
    :align: center

    A KJV Sample Page Rotated With Serif Font

我們真的很喜歡莎草紙體，如同喜歡適合聖經的幾個書法字體：楷書，隸書等等。

.. _fig-BibleLayoutEngPapyrusV:
.. figure:: _static/images/BibleLayoutEngPapyrusV.jpg
    :width: 455px
    :figwidth: 455px
    :alt: A KJV Sample Page Rotated with Papyrus Font
    :align: center

    A KJV Sample Page Rotated With Papyrus Font

帳號功能
=======================
直讀聖經的底層是一個雲端服務，對不具名用戶提供基本的閱覽功能，註冊為會員之後，我們才有依據為您存儲個人設定，目前最重要的是離線聖經版本的下載，將來還會陸續提供更多個人功能。
從讀經頁面上輕按一下，可以帶出主功能表，帳號功能在右上角的三條橫線圖示（英文俗稱 the hamburger menu icon，功能表漢堡），如下圖。按下『帳號』字樣以拉開其下各選項。

.. _fig-BibleAccounts:
.. figure:: _static/images/BibleAccounts.jpg
    :width: 520px
    :figwidth: 520px
    :alt: The Account Menu
    :align: center

    帳號功能表

下載經文離線閱讀
===========================

我們的想法和您一樣，下載聖經以便在沒有網路連線的時候閱讀，比是不是會員有沒有登入重要得多。好的，我們先來看看怎麼下載，首先，在帳號功能表裏按一下『下載』選項，進入下載的畫面。在還沒有登入的狀態下，您會看到下圖左邊的畫面，已登入時，這是右邊的畫面。

.. _fig-BibleDownloads:
.. figure:: _static/images/BibleDownloads.jpg
    :width: 520px
    :figwidth: 520px
    :alt: The Download Page
    :align: center

    下載頁面

如畫面上的說明，未註冊不具名的用戶，可以下載三個語系的主要版本之一，留作離線閱讀。包括，繁體和合本，簡體和合本，和英皇欽定本。註冊並登入的會員可以在全部聖經中一次選三本下載。您可能會好奇為什麼有這樣的規定，而不是全部開放。這部分就需要各位用戶的理解了，雖然直讀聖經提供免費的服務，但是其實我們自己按月支付雲端設備與服務的租金。其中資料流量本身是一個變動的支出：當很多人下載很多次很多部聖經的時候，我們會收到相對高額的帳單，所以不能無限量供應，是可能因此破產的。如果有點難理解，您可以想一下出國旅行時，漫遊上網沒有設限的後果，回來您可能收到一張好幾倍於當次旅費的通訊費用帳單。所以這裡的會員規範，雖然不算什麼福利，但卻是我們能維持運作的一個關鍵。請務必支持配合。
回到下載的操作，當您選定版本按確定之後，程式會開始幫您下載所選聖經，整部66卷經書全部下載到您的手機，平板，或電腦。您會看到畫面下方有下載進度百分比的顯示。除非您的網路狀況很不理想，通常數十秒到幾分鐘之內可以下載完畢。下載完畢後，進度顯示為 100％，然後原地出現一個通知訊息（綠色的英文訊息），告訴您下載完畢，並且開啟剛剛下載的版本 （Download Complete. Reloaded to XXV）。

.. _fig-BibleDownloadProgress:
.. figure:: _static/images/BibleDownloadProgress.jpg
    :width: 260px
    :figwidth: 260px
    :alt: The Download Progress
    :align: center

    下載進度

.. _fig-BibleDownloadComplete:
.. figure:: _static/images/BibleDownloadComplete.jpg
    :width: 260px
    :figwidth: 260px
    :alt: Download Complete Message
    :align: center

    下載完成通知

下載過程中，進度的百分比隨時更新在下方，但是也擋住了下功能表，沒錯，是故意這樣設計的。下載的時候，您仍可以繼續閱讀開始下載前的經文版面，但是，不能操作版本書卷篇章等項目的更換：避免不必要的錯亂與干擾。（餐廳裡通常沒有人邊結帳邊點菜的啦！）因為您可能從天涯海角任何一個地方上線進來，連線品質是我們無法預期的，所以如果基於任何原因不幸下載失敗了，一直沒看到下載完成的訊息，那麼就請您再操作一次。

- \ **離線狀態**\

下載成功後，您什麼也不必做，只是在您沒有連網的時候開啟直讀聖經時，程式會自動開放已下載的版本，供您閱讀。手機上沒有儲存離線資料的版本，在選單裡會被灰掉，不能按。這裡算是直讀聖經一個聰明的設計，當您的裝置可以連線上網的時候，所有需要連線的功能與選項全部開啟；當您沒有網路連線的時候，所有需要連線的功能與選項全部關閉。如下圖左，離線時，主控頁，下載，更多資訊等功能會被自動關閉；下圖右，離線時，只有已完整下載過的版本可以選擇。如果您真的很想模擬以下，可以把手機或平板切到飛行模式，這時候所有的數據連線全部關閉，您在打開直讀聖經操作看看，您不必學任何新的的操作，只是部分功能被關閉而已。但是，如果您沒有下載過任何一個版本，那麼一離線就什麼都看不到了喔。

.. _fig-BibleOffline:
.. figure:: _static/images/BibleOffline.jpg
    :width: 530px
    :figwidth: 530px
    :alt: Offline
    :align: center

    離線狀態

登入與註冊
===========================
如果您正使用一支蘋果或安卓手機，那麼應該是註冊登入登出過很多服務與帳號了，所以我們就不用多作解釋。帳號功能表的最底下一項是登入，畫面長這樣：

.. _fig-BibleAccountLogin:
.. figure:: _static/images/BibleAccountLogin.jpg
    :width: 260px
    :figwidth: 260px
    :alt: Login
    :align: center

    登入介面

如果您還沒有帳號，登如畫面上有『註冊』兩個字，按下去，進入註冊畫面，填寫資料，即可註冊。

.. _fig-BibleAccountRegister:
.. figure:: _static/images/BibleAccountRegister.jpg
    :width: 260px
    :figwidth: 260px
    :alt: Register
    :align: center

    註冊介面

註冊資料送出後，遠端的主機會發出一封確認信到您所填的 email，通常您可以馬上收到。請打開確認信，按其中的確認連結。該連結會再帶您回到直讀聖經的網頁，感謝您的加入一番後，可以繼續開始操作。直讀聖經並沒有要使用您的電子郵件做什麼，但是前面提過了，這是一個防護機制，避免惡意的大量下載癱瘓等類的網路攻擊，保護我們的租金帳單。

- \ **主控頁**\

主控頁目前並沒有特別的功能，主要顯示您帳號相關的資訊。當更多的會員功能加進來之後，都會從這個頁面設定或開關。主控頁現在長這個樣子：

.. _fig-BibleAccountDashboad:
.. figure:: _static/images/BibleAccountDashboard.jpg
    :width: 260px
    :figwidth: 260px
    :alt: Dashboard
    :align: center

    主控頁

主控頁上面有一個『修改』按鍵，按下去可以填寫或修改您的個人資訊。我們理解每個人都有自己的隱私考量，未經同意，不會公開這些資訊在任何地方。萬一您對於現今的網路安全沒有十足的信心，我們仍建議您盡可能如實填寫聯絡方式，但避開敏感項目。例如，你不想讓任何人知道本名（那麼請給我們一個稱呼），手機號碼或住家地址，有顧慮都可以不填。但是請讓我們在必要的時候用，至少能透過 email 與您聯繫上，不要透過任何用過即丟的郵件信箱。畢竟，我們都是主內肢體，願能以基本的誠信互動。

好的，個人資料頁面長這樣。我們調皮了一下，下圖是剪接的截圖，我們手邊並沒有螢幕這麼細長的手機，只是想讓您容易閱讀。在您正常的手機，往下滑，就可以看到完整的頁面了。

.. _fig-BibleAccountProfile:
.. figure:: _static/images/BibleAccountProfile.jpg
    :width: 260px
    :figwidth: 260px
    :alt: Profile
    :align: center

    個人資訊頁面

與我們一起成長
=======================
到這裡，使用說明算是寫完了。願您讀經愉快，在主的話語裡隨時得到亮光與平安。
任何問題與建議都歡迎以電郵聯繫。

.. figure:: _static/images/supportEmail.png
    :width: 300px
    :figwidth: 300px
    :align: center