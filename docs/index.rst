.. Read the Docs Template documentation master file, created by
   sphinx-quickstart on Tue Aug 26 14:19:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

歡迎使用直讀聖經
==================================================

目錄:

.. toctree::
   :maxdepth: 2

   features
   installation
   usage
