============
網址與安裝
============

網頁版：
====================
- 開啟網頁即可閱讀
- 網址：`直讀聖經 <https://www.bibles.com.tw>`_
- 支援瀏覽器（通過測試）
    - Safari: 10
    - Chrome: 61
    - FireFox: 56
    - IE: 10
    - Edge: 15
    - Opera: 47
- 多數早於上列版本之瀏覽器仍可運作，少部分顯示以及離線功能可能不完整。建議透過各瀏覽器的自動更新功能，將瀏覽器更新至最新版，以便利用各項新功能。

iOS 版：
====================
- `Apple App Store <https://itunes.apple.com/tw/app/直讀聖經/id1287370823>`_
- 最新版：V1.01
- 支援：iOS 8.0+, 包含 iPhone, iPad, iPod touch
- 免費安裝

.. figure:: _static/images/DirectBiblesAppStore.jpg
    :width: 300px
    :alt: Direct Bibles iOS Download
    :align: center
    :target: https://itunes.apple.com/tw/app/直讀聖經/id1287370823

    Apple App 商店

Android 版：
====================
- `Google Play Store <https://play.google.com/store/apps/details?id=tw.com.bibles.chinesebible>`_
- 最新版：V1.10
- 支援：Android 5.0+
- 免費安裝

.. figure:: _static/images/DirectBiblesPlayStore.jpg
    :width: 300px
    :alt: Direct Bibles Android Download
    :align: center
    :target: https://play.google.com/store/apps/details?id=tw.com.bibles.chinesebible

    Google Play 商店
